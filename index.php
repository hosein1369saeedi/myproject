<!DOCTYPE html>
<html lang="en">

    <?php include("blocks/head.php");?>

    <body>

        <?php include("blocks/menu.php");?>
        <?php include("blocks/slider.php");?>
        <?php include("blocks/services.php");?>
        <?php include("blocks/sample-e.php");?>
        <?php include("blocks/footer.php");?>

        <?php include("blocks/script.php");?>

    </body>
    
</html>