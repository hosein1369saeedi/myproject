<!DOCTYPE html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <?php include("blocks/internalpage.php");?>
        <br>
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <ul>
                        <li class="homebreadcrumbs">
                            <a href="index.php">خانه</a> <span class="flaticon flaticon-left-arrow"></span>
                        </li>
                        <li>
                            <a href="contact.php">تماس با ما</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section class="contact">
            <div class="thm-container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="contact-form-content">
                            <div class="title text-right">
                                <span>فرم تماس با ما</span>
                            </div><!-- /.title -->
                            <form action="inc/sendemail.php" class="contact-form" novalidate="novalidate">
                                <input type="text" name="name" placeholder="نام و نام خانوادگی">
                                <input type="text" name="mobile" placeholder="شماره همراه">
                                <textarea name="message" placeholder="پیام خود را بنویسید"></textarea>
                                <button type="submit" class="thm-btn yellow-bg">ارسال پیام</button>
                                <div class="form-result"></div><!-- /.form-result -->
                            </form>
                        </div><!-- /.contact-form-content -->
                    </div><!-- /.col-md-8 -->
                    <div class="col-md-4">
                        <div class="contact-info text-right">
                            <div class="title text-right">
                                <span>راه های تماس با ما</span>
                            </div><!-- /.title -->
                            <div class="single-contact-info">
                                <h4>شماره تلفن</h4>
                                <p>موبایل : ۰۹۱۹<br> موبایل : 09394109462</p>
                            </div><!-- /.single-contact-info -->
                            <div class="single-contact-info">
                                <h4>ایمیل</h4>
                                <p>needhelp@printify.com <br> inquiry@printify.com</p>
                            </div><!-- /.single-contact-info -->
                            <div class="single-contact-info">
                                <h4>راه های دیگر</h4>
                                <div class="social">
                                <a href=""><img src="images/png/001-telegram.png" alt=""></a>
                                <a href=""><img src="images/png/002-whatsapp.png" alt=""></a>
                                <a href=""><img src="images/png/005-instagram.png" alt=""></a>
                                </div><!-- /.social -->
                            </div><!-- /.single-contact-info -->
                        </div><!-- /.contact-info -->
                    </div><!-- /.col-md-4 -->
                </div><!-- /.row -->
            </div>
        </section>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>