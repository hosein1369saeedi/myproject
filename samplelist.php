<!DOCTYPE html>
<html lang="en">
    <?php include("blocks/head.php");?>
    <body>
        <?php include("blocks/menu.php");?>
        <?php include("blocks/internalpage.php");?>
        <br>
        <section class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <ul>
                        <li class="homebreadcrumbs">
                            <a href="index.php">خانه</a> <span class="flaticon flaticon-left-arrow"></span>
                        </li>
                        <li>
                            <a href="sample.php">نمونه کار</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <?php include("blocks/footer.php");?>
        <?php include("blocks/script.php");?>
    </body>
</html>