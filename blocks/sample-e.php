<section class="sample-e">
    <h2 class="alphabet credwh text-center h2"><img src="images/logo/interior design(3).png" alt="" style="width:3%"> نمونه کار</h2>
    <img src="images/qwqw.png" alt="" style="width:20%">
    <hr>
    <br>
    <div class="mt-40">
        <div class="row mt-30">
            <div class="col-sm-12 col-md-4">
                <div class="box20 blue">
                    <img src="images/sample/sample-3.png" alt="">
                    <div class="box-content">
                        <h3 class="title text-right">سایت فروشگاهی ' گلدن۸ '</h3>
                        <p>
                            <a class="button" href="sampledetails.php">مشاهده جزییات</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="box20 blue">
                    <img src="images/sample/sample-2.png" alt="">
                    <div class="box-content">
                        <h3 class="title text-right">سایت شرکتی ' پارس ترونیک '</h3>
                        <p>
                            <a class="button" href="sampledetails.php">مشاهده جزییات</a>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-4">
                <div class="box20 blue">
                    <img src="images/sample/sample-1.png" alt="">
                    <div class="box-content">
                        <h3 class="title text-right">سایت آموزشی ' ستارک '</h3>
                        <p>
                            <a class="button" href="sampledetails.php">مشاهده جزییات</a>
                        </p>
                    </div>
                </div>
            </div>
            <a href="samplelist.php" class="sample-a">مشاهده بیشتر...</a>
        </div>
    </div>
</section>