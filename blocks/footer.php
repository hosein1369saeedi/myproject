<section class="footer">
    <div class="search-text"> 
        <div class="container">
            <div class="row text-center">
                <div class="form" style="width: 100%;">
                    <form id="search-form" class="form-search form-horizontal">
                        <input type="text" class="input-search" placeholder="دنبال چی میگردی؟">
                        <button type="submit" class="btn-search">جستجو</button>
                    </form>
                </div>
            </div>         
        </div>     
    </div>
    <footer>
        <div class="container">
            <div class="row text-center">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="menu list-inline">
                        <li><a href="">نمونه کار</a></li>
                        <li><a href="">گالری</a></li>
                        <li><a href="">تماس با ما</a></li>
                        <li><a href="">درباره ما</a></li>
                    </ul>
                </div>
            </div> 
        </div>
    </footer>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="row text-center">
                        <p>طراحی : <a href=""><img src="images/logo/interior design(1).png" alt="" style="width:15%"></a></p>
                    </div>
                </div>
                <div class="col-md-6">
                    <ul class="list-inline text-center" style="margin: 15px 0px;">
                        <li><a href=""><img src="images/png/001-telegram.png" alt=""></a></li>
                        <li><a href=""><img src="images/png/002-whatsapp.png" alt=""></a></li>
                        <li><a href=""><img src="images/png/005-instagram.png" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>