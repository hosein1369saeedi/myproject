<section class="menu">
    <div id="myNav" class="overlay">
        <a href="index.html" style="margin:0.5%;margin-left: 2%;">
            <span class="flaticon flaticon-scientist" style="color:#fff;font-size: 3vw"></span>
        </a>
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">
            <span class="flaticon flaticon-cancel"></span>
        </a>
        <div class="overlay-content">
            <a href="index.php">خانه</a>
            <a href="gallery.php">گالری</a>
            <a href="samplelist.php">نمونه کار</a>
            <a href="contact.php">تماس با ما</a>
            <a href="about.php">درباره ما</a>
        </div>
    </div>
    <span class="iconnav"onclick="openNav()">
        <span class="flaticon flaticon-menu"></span>
    </span>
    <div class="login">
        <ul>
            <li>
                <a href="" data-toggle="tooltip" title="ثبت نام">
                    <span class="flaticon flaticon-keyhole"></span>
                </a>
            </li>
            <li>
                <a href="" data-toggle="tooltip" title="ورود">
                    <span class="flaticon flaticon-user"></span>
                </a>
            </li>
        </ul>
    </div>
</section>