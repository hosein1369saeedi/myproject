<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="images/logo/interior design(2).png">

    <link href="css/bootstrap.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="css/bootstrap-grid.min.css" rel="stylesheet" crossorigin="anonymous">
    <link href="css/animate.css" rel="stylesheet" crossorigin="anonymous">
    <link href="fonts/myicons/flaticon.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet" crossorigin="anonymous">
</head>